# level2-remix Ember Plugin
This is a level2-ember plugin that provides a basic interface to the level2-remix framework.
It is used internally by the level2-remix framework for the admin user interface.
This plugin is only compatible with the latest level2-remix version.