define('component:l2-ui-select', function() {
	return Ember.Component.extend({
		
		// value is the value from the select input, init value is set on template.
		value: null,
		
		// placeholder is the placeholder text, shown before a value is selected.
		placeholder: null,
		
		// search is a flag to enable select searching.
		search: false,
		
		// Internal properties
		tagName: 'div',
		classNameBindings: ['selectClass'],
		
		// selectClass is the CSS class name for the select element.
		selectClass: Ember.computed('search', function() {
			var uiClass = 'ui ';
			
			if(this.get('search') === true) {
				uiClass += 'search ';
			}
			
			return uiClass + 'selection dropdown';
		}),
		
		// didInsertElement initializes the dropdown element.
		didInsertElement: function() {
			this.$().dropdown();
		}
		
	});
});