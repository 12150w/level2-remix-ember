/*
	Loader
	
	A Loader assists in loading properties asynchronously from a server.
	It is used to simplify the variables associated with the loading process.
	
	Using the loader can be done via 2 methods:
		1) Using Loader.load(promise)
		2) Using Loader.create({promise: ...})
	Either way requires passing in a promise but the second way allows setting more options if needed.
*/
define('l2:loader', function() {
	var Loader = Ember.Object.extend({
		
		// promise is the promise chain the Loader loads (it is required).
		promise: null,
		
		// loading is true when the Loader is currently loading data, otherwise it is false.
		loading: false,
		
		// result is the data that the promise was resolved with, or null if the promise failed or is loading
		result: null,
		
		// error is the error the promise failed with, or null if the promise succeeded or is loading
		error: null,
		
		// done is true if the loader is not loading currently (opposite of loading)
		done: function() {
			return !this.get('loading');
		}.property('loading'),
		
		// init sets up handlers on the promise chain to control the Loader
		init: function() {
			this._super.apply(this, arguments);
			
			var promise = this.get('promise');
			if(promise == null) {
				throw new Error('Loader.promise can not be blank');
			}
			if(typeof promise.then !== 'function') {
				throw new Error('Loader.promise must be a promise object (containing a promise.then() function)');
			}
			
			var self = this;
			self.set('loading', true);
			promise.then(function(result) {
				// Handle success
				self.set('loading', false);
				self.set('result', result);
				
			}, function(err) {
				// Handle error
				self.set('loading', false);
				self.set('error', err);
				
			});
		}
		
	});
	
	// Loader.load is a convinience method for creating a Loader with a certain promise.
	Loader.load = function(promise) {
		return Loader.create({
			promise: promise
		});
	};
	
	return Loader;
});