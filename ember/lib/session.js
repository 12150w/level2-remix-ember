/*
	Session
	
	Contains the context of the current user session and provides a way to reliably
	send HTTP requests to level2-remix and parse errors.
*/
define('l2:session', ['l2:httpError'], function(HTTPError) {
	return Ember.Object.extend({
		
		// loading is true if there is an active session request.
		loading: false,
		
		// uid is the id of currently logged in user, or null if not logged in
		uid: null,
		
		// sid is the session id of the current session, or null if not logged in
		sid: null,
		
		// uidRegex is the regex to find the UID from the cookie (must contain 1 group)
		uidRegex: /l2uid=(\d+)/,
		
		// sidRegex is the regex to find the SID from the cookie (must contain 1 group)
		sidRegex: /l2sid=([^;]+)/,
		
		// active is true if the user is currently logged in
		active: function() {
			return this.get('uid') != null;
		}.property('uid'),
		
		// getJSON sends an HTTP(S) GET request, returns a promise for the deserialized result.
		getJSON: function(url) {
			return this.request({
				method: 'GET',
				url: url,
				dataType: 'json'
			});
		},
		
		// postJSON sends an HTTP(S) POST request with a JSON body, returns a promise for the deserialized result.
		postJSON: function(url, data) {
			if(typeof data !== 'string') {
				data = JSON.stringify(data);
			}
			
			return this.request({
				method: 'POST',
				url: url,
				contentType: 'application/json',
				dataType: 'json',
				data: data
			});
		},
		
		// putJSON sends an HTTP(S) PUT request with a JSON body, returns a promise for the deserialized result.
		putJSON: function(url, data) {
			if(typeof data !== 'string') {
				data = JSON.stringify(data);
			}
			
			return this.request({
				method: 'PUT',
				url: url,
				contentType: 'application/json',
				dataType: 'json',
				data: data
			});
		},
		
		// deleteJSON sends an HTTP(S) DELETE request with a JSON body, returns a promise for the deserialized result.
		deleteJSON: function(url, data) {
			if(typeof data !== 'string') {
				data = JSON.stringify(data);
			}
			
			return this.request({
				method: 'DELETE',
				url: url,
				contentType: 'application/json',
				dataType: 'json',
				data: data
			});
		},
		
		// request sends an HTTP(S) request, returning a promise for the result.
		// This will verify any active session headers are added to the request.
		request: function(options) {
			var self = this,
				ajaxDefer = Ember.$.Deferred();
			options = options || {};
			
			// Add the Authorization header
			options.headers = options.headers || {};
			if(!!this.get('sid')) options.headers['Authorization'] = this.get('sid');
			
			// Send the request
			this.set('loading', true);
			Ember.$.ajax(options).always(function() {
				self.set('loading', false);
				
			}).fail(function(err) {
				ajaxDefer.reject(new HTTPError(err));
				
			}).done(function(res) {
				ajaxDefer.resolve(res);
				
			});
			
			return ajaxDefer;
		},
		
		// refresh reloads the SID and UID values from the cookies
		//   forcedSID is a forced SID to use (instead of reading the SID from the cookies)
		refresh: function(forcedSID) {
			var uidMatch = this.get('uidRegex').exec(document.cookie);
			if(uidMatch != null) {
				this.set('uid', uidMatch[1]);
			} else {
				this.set('uid', null);
			}
			
			if(!!forcedSID) {
				this.set('sid', forcedSID);
			} else {
				var sidMatch = this.get('sidRegex').exec(document.cookie);
				if(sidMatch != null) {
					this.set('sid', sidMatch[1]);
				} else {
					this.set('sid', null);
				}
			}
		},
		
		// reset clears the uid and sid of this session making it inactive (but does not clear the cookies)
		reset: function() {
			this.set('sid', null);
			this.set('uid', null);
		},
		
		// init sets up the session from cookies
		init: function() {
			this.refresh();
		}
		
	});
});