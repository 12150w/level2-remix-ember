/*
	HTTP Error
	
	Represents server errors from a failed request
*/

define('l2:httpError', function() {
	function SessionError(res) {
		
		// statusCode is the HTTP status code of the error
		this.statusCode = res.status;
		
		// needsAuthentication is a flag that indicates that this error arose from needing authentication
		this.needsAuthentication = res.status === 401;
		
		// Check for JSON error
		if(!!res.responseJSON) {
			var body = res.responseJSON;
			
			if(!!body.msg) {
				this.message = body.msg;
			} else {
				this.message = JSON.stringify(body);
			}
			
			if(!!body.code) {
				this.errorCode = body.code;
			}
			
			if(!!body.stack) {
				this.details = body.stack.replace(/\\n/g, '<br />');
			}
			return;
		}
		
		this.message = res.responseText;
	};
	
	SessionError.prototype = new Error();
	SessionError.prototype.name = 'Server Error';
	
	return SessionError;
});
