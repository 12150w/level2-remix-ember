define('initializer:l2AdminSession', ['l2:session'], function(Session) {
	return Ember.Application.initializer({
		name: 'l2AdminSession',
		initialize: function(app) {
			app.register('l2Session:admin', Session.extend({
				uidRegex: /l2uid_adm=(\d+)/,
				sidRegex: /l2sid_adm=([^;]+)/,
				
				active: Ember.computed('sid', function() {
					return !!this.get('sid');
				})
				
			}));
			
			app.inject('controller', 'adminSession', 'l2Session:admin');
			app.inject('route', 'adminSession', 'l2Session:admin');
			app.inject('component', 'adminSession', 'l2Session:admin');
		}
	});
});