define('initializer:l2Session', ['l2:session'], function(Session) {
	return Ember.Application.initializer({
		name: 'l2Session',
		initialize: function(app) {
			app.register('l2Session:main', Session);
			
			app.inject('controller', 'session', 'l2Session:main');
			app.inject('route', 'session', 'l2Session:main');
			app.inject('component', 'session', 'l2Session:main');
		}
	});
});